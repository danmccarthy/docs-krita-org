# Translation of docs_krita_org_user_manual___introduction_from_other_software.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: user_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-10 12:47+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../user_manual/introduction_from_other_software.rst:5
msgid "Introduction Coming From Other Software"
msgstr "Introducció venint des d'un altre programari"

#: ../../user_manual/introduction_from_other_software.rst:7
msgid ""
"Krita is not the only digital painting application in the world. Because we "
"know our users might be approaching Krita with their experience from using "
"other software, we have made guides to illustrate differences."
msgstr ""
"El Krita no és l'única aplicació de pintura digital del món. Perquè sabem "
"que els nostres usuaris podrien acostar-se al Krita amb la seva experiència "
"emprant un altre programari, hem fet guies per il·lustrar les diferències."

#: ../../user_manual/introduction_from_other_software.rst:10
msgid "Contents:"
msgstr "Contingut:"
