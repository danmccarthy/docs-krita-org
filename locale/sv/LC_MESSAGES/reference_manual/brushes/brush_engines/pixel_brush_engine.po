# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-15 03:16+0200\n"
"PO-Revision-Date: 2019-06-15 08:22+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:1
msgid "The Pixel Brush Engine manual page."
msgstr "Manualsidan för bildpunktspenselgränssnittet."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:11
#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:16
msgid "Pixel Brush Engine"
msgstr "Bildpunktspenselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:11
msgid "Brush Engine"
msgstr "Penselgränssnitt"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:19
msgid ".. image:: images/icons/pixelbrush.svg"
msgstr ".. image:: images/icons/pixelbrush.svg"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:20
msgid ""
"Brushes are ordered alphabetically. The brush that is selected by default "
"when you start with Krita is the :guilabel:`Pixel Brush`. The pixel brush is "
"the traditional mainstay of digital art. This brush paints impressions of "
"the brush tip along your stroke with a greater or smaller density."
msgstr ""
"Penslar är ordnade alfabetiskt. Penseln som normalt är vald när man startar "
"Krita är  :guilabel:`Bildpunktspensel`. Bildpunktspenselen är den "
"traditionella grundstenen i digital kost. Penseln målar avtryck av spetsen "
"längs penseldraget med en större eller mindre täthet."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:24
msgid ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Popup.png"
msgstr ".. image:: images/brushes/Krita_Pixel_Brush_Settings_Popup.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:25
msgid "Let's first review these mechanics:"
msgstr "Låt oss först granska själva mekaniken:"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:27
msgid ""
"Select a brush tip. This can be a generated brush tip (round, square, star-"
"shaped), a predefined bitmap brush tip, a custom brush tip or a text."
msgstr ""
"Välj en penselspets. Det kan vara en genererad penselspets (rund, "
"fyrkanting, stjärnformat), en fördefinierad bitavbildad penseltips, en egen "
"penselspets eller en text."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:28
msgid ""
"Select the spacing: this determines how many impressions of the tip will be "
"made along your stroke"
msgstr ""
"Välj mellanrum: det bestämmer hur många avtryck av spetsen som görs längs "
"draget"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:29
msgid ""
"Select the effects: the pressure of your stylus, your speed of painting or "
"other inputs can change the size, the color, the opacity or other aspects of "
"the currently painted brush tip instance -- some applications call that a "
"\"dab\"."
msgstr ""
"Välj effekterna: pennans tryck, hastigheten man målar eller annan inmatning "
"kan ändra storleken, färgen, ogenomskinligheten eller andra aspekter av den "
"nuvarande penselspetsens instans - vissa program kallar det ett \"klick\"."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:30
msgid ""
"Depending on the brush mode, the previously painted brush tip instance is "
"mixed with the current one, causing a darker, more painterly stroke, or the "
"complete stroke is computed and put on your layer. You will see the stroke "
"grow while painting in both cases, of course!"
msgstr ""
"Beroende på penselläge, blandas den tidigare penselspetsens instans med den "
"nuvarande, vilket orsakar ett mörkare, mer målarliknande drag, eller så "
"beräknas hela draget och läggs till på lagret. Man ser förstås penseldraget "
"växa medan det målas i båda fallen."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:32
msgid ""
"Since 4.0, the Pixel Brush Engine has Multithreaded brush-tips, with the "
"default brush being the fastest mask."
msgstr ""
"Sedan 4.0 har bildpunktspenselgränssnittet penselspetsar med flera "
"beräkningstrådar, där standardpenseln har den snabbaste masken."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:34
msgid "Available Options:"
msgstr "Tillgängliga alternativ:"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:36
msgid ":ref:`option_brush_tip`"
msgstr ":ref:`option_brush_tip`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:37
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:38
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:39
msgid ":ref:`option_size`"
msgstr ":ref:`option_size`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:40
msgid ":ref:`option_ratio`"
msgstr ":ref:`option_ratio`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:41
msgid ":ref:`option_spacing`"
msgstr ":ref:`option_spacing`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:42
msgid ":ref:`option_mirror`"
msgstr ":ref:`option_mirror`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:43
msgid ":ref:`option_softness`"
msgstr ":ref:`option_softness`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:44
msgid ":ref:`option_sharpness`"
msgstr ":ref:`option_sharpness`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:45
msgid ":ref:`option_rotation`"
msgstr ":ref:`option_rotation`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:46
msgid ":ref:`option_scatter`"
msgstr ":ref:`option_scatter`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:47
msgid ":ref:`option_source`"
msgstr ":ref:`option_source`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:48
msgid ":ref:`option_mix`"
msgstr ":ref:`option_mix`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:49
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:50
msgid ":ref:`option_texture`"
msgstr ":ref:`option_texture`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:51
msgid ":ref:`option_masked_brush`"
msgstr ":ref:`option_masked_brush`"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:54
msgid "Specific Parameters to the Pixel Brush Engine"
msgstr "Specifika parametrar för bildpunktspenselgränssnittet"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:57
msgid "Darken"
msgstr "Gör mörkare"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:59
msgid "Allows you to Darken the source color with Sensors."
msgstr "Låter dig göra källfärgen mörkare med sensorer."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:62
msgid ".. image:: images/brushes/Krita_2_9_brushengine_darken_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_darken_01.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:63
msgid ""
"The color will always become black in the end, and will work with Plain "
"Color, Gradient and Uniform random as source."
msgstr ""
"Färgen blir alltid svart till slut, och fungerar med enkelt färg, toning och "
"likformigt slumpmässig färg som källa."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:66
msgid "Hue, Saturation, Value"
msgstr "Färgton, mättnad, värde"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:68
msgid ""
"These parameters allow you to do an HSV adjustment filter on the :ref:"
"`option_source` and control it with Sensors."
msgstr ""
"Parametrarna låter dig göra ett HSV-justeringsfilter för  :ref:"
"`option_source` och kontrollera det med sensorer."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:71
msgid ".. image:: images/brushes/Krita_2_9_brushengine_HSV_01.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_HSV_01.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:72
msgid "Works with Plain Color, Gradient and Uniform random as source."
msgstr ""
"Fungerar med enkelt färg, toning och likformigt slumpmässig färg som källa."

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:75
msgid "Uses"
msgstr "Använder"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:78
msgid ".. image:: images/brushes/Krita_2_9_brushengine_HSV_02.png"
msgstr ".. image:: images/brushes/Krita_2_9_brushengine_HSV_02.png"

#: ../../reference_manual/brushes/brush_engines/pixel_brush_engine.rst:79
msgid ""
"Having all three parameters on Fuzzy will help with rich color texture. In "
"combination with :ref:`option_mix`, you can have even finer control."
msgstr ""
"Att ställa in alla tre parametrar till suddig hjälper till med rik "
"färgstruktur. Tillsammans med :ref:`option_mix` kan man få ännu finare "
"kontroll."
