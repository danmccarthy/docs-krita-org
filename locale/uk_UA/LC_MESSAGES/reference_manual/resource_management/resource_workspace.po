# Translation of docs_krita_org_reference_manual___resource_management___resource_workspace.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___resource_management___resource_workspace\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-04 11:27+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/resource_management/resource_workspace.rst:1
msgid "Managing workspaces and sessions in Krita."
msgstr "Керування робочими просторами і сеансами у Krita."

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:16
msgid "Workspaces"
msgstr "Робочі простори"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:25
msgid "Window Layouts"
msgstr "Компонування вікна"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:37
msgid "Sessions"
msgstr "Сеанси"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
msgid "Resources"
msgstr "Ресурси"

#: ../../reference_manual/resource_management/resource_workspace.rst:18
msgid ""
"Workspaces are basically saved configurations of dockers.  Each workspace "
"saves how the dockers are grouped and where they are placed on the screen.  "
"They allow you to easily move between workflows without having to manual "
"reconfigure your setup each time.  They can be as simple or as complex as "
"you want."
msgstr ""
"Робочі простори — збережені налаштування бічних панелей. У даних кожного "
"робочого простору зберігається спосіб групування бічних панелей та їхнє "
"розташування на екрані. Ви можете без проблем міняти простори назв без "
"потреби у ручному повторному налаштовуванні програми після кожної зміни. "
"Робочі простори можуть бути простими або як завгодно складними."

#: ../../reference_manual/resource_management/resource_workspace.rst:20
msgid ""
"Workspaces can only be accessed via the toolbar or :menuselection:`Window --"
"> Workspaces` There's no docker for them.  You can save workspaces, in which "
"your current configuration is saved. You can also import them (from a \\*."
"kws file), or delete them (which black lists them)."
msgstr ""
"Доступ до робочих просторів можна отримати лише за допомогою панелі "
"інструментів або пункту меню :menuselection:`Вікно --> Робочі простори`. Для "
"них не передбачено спеціальної бічної панелі. Ви можете зберігати робочі "
"простори із поточними налаштуваннями. Ви також можете імпортувати їх (з "
"файлів \\*.kws) або вилучати їх (тобто додавати їх до «чорного» списку)."

#: ../../reference_manual/resource_management/resource_workspace.rst:22
msgid ""
"Workspaces can technically be tagged, but outside of the resource manager "
"this is not possible."
msgstr ""
"З технічної точки зору, робочі простори можна позначати мітками, але поза "
"панеллю керування ресурсами це неможливо."

#: ../../reference_manual/resource_management/resource_workspace.rst:27
msgid ""
"When you work with multiple screens, a single window with a single workspace "
"won't be enough. For multi monitor setups we instead can use sessions. "
"Window layouts allow us to store multiple windows, their positions and the "
"monitor they were on."
msgstr ""
"Якщо ви працюєте за декількома екранами, одного вікна із одним робочим "
"простором може бути недостатньо. Для багатомоніторних конфігурацій варто "
"використовувати сеанси. Компонування вікон надає змогу зберігати стан "
"декількох вікон, їхнє розташування та дані щодо монітора, на якому їх "
"показано."

#: ../../reference_manual/resource_management/resource_workspace.rst:29
msgid ""
"You can access Window Layouts from the workspace drop-down in the toolbar."
msgstr ""
"Доступ до компонувань вікон можна отримати за допомогою спадного списку "
"робочих просторів на панелі інструментів."

#: ../../reference_manual/resource_management/resource_workspace.rst:31
msgid "Primary Workspace Follows Focus"
msgstr "Основна робоча область слідує за фокусом"

#: ../../reference_manual/resource_management/resource_workspace.rst:32
msgid ""
"This treats the workspace in the first window as the 'primary' workspace, "
"and when you switch focus, it will switch the secondary windows to that "
"primary workspace. This is useful when the secondary workspace is a very "
"sparse workspace with few dockers, and the primary is one with a lot of "
"different dockers."
msgstr ""
"Позначення цього пункту призведе до того, що програма вважатиме робочий "
"простір у першому вікні «основним» робочим простором. Коли ви перемикатимете "
"фокус, програма перемикатиме вікна до цього основного робочого простору. Це "
"корисно, якщо вторинний робочий простір містить лише декілька бічних "
"панелей, а у основному дуже багато різних бічних панелей."

#: ../../reference_manual/resource_management/resource_workspace.rst:34
msgid "Show Active Image In All Windows"
msgstr "Показувати активне зображення у всіх вікнах"

#: ../../reference_manual/resource_management/resource_workspace.rst:34
msgid ""
"This will synchronise the currently viewed image in all windows. Without it, "
"different windows can open separate views for an image via :menuselection:"
"`Window --> New View --> document.kra`."
msgstr ""
"За допомогою цього пункту можна синхронізувати поточне зображення у всіх "
"вікнах. Без його позначення можна відкривати окремі панелі перегляду "
"зображення за допомогою пункту :menuselection:`Вікно --> Нова панель "
"перегляду --> документ.kra`."

#: ../../reference_manual/resource_management/resource_workspace.rst:39
msgid ""
"Sessions allow Krita to store the images and windows opened. You can tell "
"Krita to automatically save current or recover previous sessions if so "
"configured in the :ref:`misc_settings`."
msgstr ""
"Файли сеансів надають змогу Krita зберігати дані щодо відкритих зображень та "
"вікон. Ви можете наказати Krita автоматично зберігати поточний сеанс або "
"відновлювати попередні сеанси, якщо налаштуєте програму відповідним чином за "
"допомогою сторінки налаштувань :ref:`misc_settings`."

#: ../../reference_manual/resource_management/resource_workspace.rst:41
msgid "You can access sessions from :menuselection:`File --> Sessions`."
msgstr ""
"Доступ до сеансів можна отримати за допомогою пункту меню :menuselection:"
"`Файл --> Сеанси`."
