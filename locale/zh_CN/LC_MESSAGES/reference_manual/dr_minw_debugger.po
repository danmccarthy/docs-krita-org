msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___dr_minw_debugger.pot\n"

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-screen.png"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-explorer-path.png"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-log-start.png"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-crash-log-end.png"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-dbg7zip.png"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:0
msgid ".. image:: images/Mingw-dbg7zip-dir.png"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:1
msgid "How to get a backtrace in Krita using the dr. MinW debugger."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:13
msgid "Backtrace"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:13
msgid "Debug"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:18
msgid "Dr. MinW Debugger"
msgstr "Dr. Mingw 调试程序"

#: ../../reference_manual/dr_minw_debugger.rst:22
msgid ""
"The information on this page applies only to the Windows release of Krita "
"3.1 Beta 3 (3.0.92) and later."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:26
msgid "Getting a Backtrace"
msgstr "获取回溯数据"

#: ../../reference_manual/dr_minw_debugger.rst:28
msgid ""
"There are some additions to Krita which makes getting a backtrace much "
"easier on Windows."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:32
msgid ""
"When there is a crash, Krita might appear to be unresponsive for a short "
"time, ranging from a few seconds to a few minutes, before the crash dialog "
"appears."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:36
msgid "An example of the crash dialog."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:38
msgid ""
"If Krita keeps on being unresponsive for more than a few minutes, it might "
"actually be locked up, which may not give a backtrace. In that situation, "
"you have to close Krita manually. Continue to follow the following "
"instructions to check whether it was a crash or not."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:40
msgid ""
"Open Windows Explorer and type ``%LocalAppData%`` (without quotes) on the "
"address bar and press the :kbd:`Enter` key."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:44
msgid ""
"Find the file ``kritacrash.log`` (it might appear as simply ``kritacrash`` "
"depending on your settings.)"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:45
msgid ""
"Open the file with Notepad and scroll to the bottom, then scroll up to the "
"first occurrence of “Error occurred on <time>” or the dashes."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:49
msgid "Start of backtrace."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:51
msgid "Check the time and make sure it matches the time of the crash."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:55
msgid "End of backtrace."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:57
msgid ""
"The text starting from this line to the end of the file is the most recent "
"backtrace."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:59
msgid ""
"If ``kritacrash.log`` does not exist, or a backtrace with a matching time "
"does not exist, then you don’t have a backtrace. This means Krita was very "
"likely locked up, and a crash didn’t actually happen. In this case, make a "
"bug report too."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:60
msgid ""
"If the backtrace looks truncated, or there is nothing after the time, it "
"means there was a crash and the crash handler was creating the stack trace "
"before being closed manually. In this case, try to re-trigger the crash and "
"wait longer until the crash dialog appears."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:64
msgid ""
"Starting from Krita 3.1 Beta 3 (3.0.92), the external DrMingw JIT debugger "
"is not needed for getting the backtrace."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:67
msgid "Using the Debug Package"
msgstr "使用调试符号包"

#: ../../reference_manual/dr_minw_debugger.rst:69
msgid ""
"Starting from 3.1 Beta 3, the debug package contains only the debug symbols "
"separated from the executables, so you have to download the portable package "
"separately too (though usually you already have it in the first place.)"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:71
msgid ""
"Links to the debug packages should be available on the release announcement "
"news item on https://krita.org/, along with the release packages. You can "
"find debug packages for any release either in https://download.kde.org/"
"stable/krita for stable releases or in https://download.kde.org/unstable/"
"krita for unstable releases. Portable zip and debug zip are found next to "
"each other."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:72
msgid ""
"Make sure you’ve downloaded the same version of debug package for the "
"portable package you intend to debug / get a better (sort of) backtrace."
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:73
msgid ""
"Extract the files inside the Krita install directory, where the sub-"
"directories `bin`, `lib` and `share` is located, like in the figures below:"
msgstr ""

#: ../../reference_manual/dr_minw_debugger.rst:79
msgid ""
"After extracting the files, check the ``bin`` dir and make sure you see the "
"``.debug`` dir inside. If you don't see it, you probably extracted to the "
"wrong place."
msgstr ""
