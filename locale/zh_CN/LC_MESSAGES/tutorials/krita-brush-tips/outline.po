msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/docs_krita_org_tutorials___krita-"
"brush-tips___outline.pot\n"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../tutorials/krita-brush-tips/outline.rst:None
msgid ""
".. image:: images/brush-tips/Krita-layerstyle_hack.png\n"
"   :alt: image demonstrating the layer style hack for this effect"
msgstr ""

#: ../../tutorials/krita-brush-tips/outline.rst:None
msgid ""
".. image:: images/brush-tips/Krita-layerstyle_hack2.png\n"
"   :alt: image demonstrating the layer style hack for this effect"
msgstr ""

#: ../../tutorials/krita-brush-tips/outline.rst:1
msgid "A tutorial about painting outline while you draw with brush"
msgstr ""

#: ../../tutorials/krita-brush-tips/outline.rst:13
msgid "Brush-tips:Outline"
msgstr "笔刷技巧：轮廓线"

#: ../../tutorials/krita-brush-tips/outline.rst:16
msgid "Question"
msgstr "问题"

#: ../../tutorials/krita-brush-tips/outline.rst:18
msgid "How to make an outline for a single brush stroke using Krita?"
msgstr ""

#: ../../tutorials/krita-brush-tips/outline.rst:20
msgid ""
"Not really a brush, but what you can do is add a layer style to a layer, by |"
"mouseright| a layer and selecting layer style. Then input the following "
"settings:"
msgstr ""

#: ../../tutorials/krita-brush-tips/outline.rst:25
msgid ""
"Then, set the main layer to multiply (or add a :ref:`filter_color_to_alpha` "
"filter mask), and paint with white:"
msgstr ""

#: ../../tutorials/krita-brush-tips/outline.rst:30
msgid ""
"(The white thing is the pop-up that you see as you hover over the layer)"
msgstr ""

#: ../../tutorials/krita-brush-tips/outline.rst:32
msgid "Merge into a empty clear layer after ward to fix all the effects."
msgstr ""
