# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-20 00:16+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en trig menuselection Tabletes Surface images Wintab\n"
"X-POFile-SpellExtra: Wacom AMD Catalyst Kritatabletstylus ref image pô\n"
"X-POFile-SpellExtra: Kritatablettypes Krita Trig Sandboxie\n"
"X-POFile-SpellExtra: Kritatabletdrivermissing SO Ink Razer guilabel Cintiq\n"

#: ../../user_manual/drawing_tablets.rst:0
msgid ".. image:: images/Krita_tablet_stylus.png"
msgstr ".. image:: images/Krita_tablet_stylus.png"

#: ../../user_manual/drawing_tablets.rst:1
msgid ""
"Basic page describing drawing tablets, how to set them up for Krita and how "
"to troubleshoot common tablet issues."
msgstr ""
"Uma página básica que descreve as tabletes de desenho, como configurá-las "
"para o Krita e como resolver problemas comuns com as tabletes."

#: ../../user_manual/drawing_tablets.rst:13
msgid "Tablets"
msgstr "Tabletes"

#: ../../user_manual/drawing_tablets.rst:18
msgid "Drawing Tablets"
msgstr "Tabletes de Desenho"

#: ../../user_manual/drawing_tablets.rst:20
msgid ""
"This page is about drawing tablets, what they are, how they work, and where "
"things can go wrong."
msgstr ""
"Esta página é dedicada às tabletes de desenho, o que são, como funcionam e "
"onde as coisas podem não correr bem."

#: ../../user_manual/drawing_tablets.rst:24
msgid "What are tablets?"
msgstr "O que são as tabletes?"

#: ../../user_manual/drawing_tablets.rst:26
msgid ""
"Drawing with a mouse can be unintuitive and difficult compared to pencil and "
"paper. Even worse, extended mouse use can result in carpal tunnel syndrome. "
"That’s why most people who draw digitally use a specialized piece of "
"hardware known as a drawing tablet."
msgstr ""
"Desenhar com um rato pode ser pouco intuitivo e difícil, em comparação com o "
"lápis e papel. Pior ainda, o uso prolongado do rato pode resultar num "
"síndroma do túnel cárpico. É por isso que a maioria das pessoas que desenham "
"a nível digital usam uma peça especializada de 'hardware', conhecida por "
"tablete de desenho."

#: ../../user_manual/drawing_tablets.rst:32
msgid ".. image:: images/Krita_tablet_types.png"
msgstr ".. image:: images/Krita_tablet_types.png"

#: ../../user_manual/drawing_tablets.rst:33
msgid ""
"A drawing tablet is a piece of hardware that you can plug into your machine, "
"much like a keyboard or mouse. It usually looks like a plastic pad, with a "
"stylus. Another popular format is a computer monitor with stylus used to "
"draw directly on the screen. These are better to use than a mouse because "
"it’s more natural to draw with a stylus and generally better for your wrists."
msgstr ""
"Uma tablete de desenho é um componente de 'hardware' que você pode ligar ao "
"seu computador, tal como um teclado ou rato. Normalmente parece uma placa de "
"plástico com um lápis. Outro formato conhecido é um monitor de computador "
"com um lápis usado para desenhar directamente no ecrã. Estes são melhores "
"para usar que um rato, porque é mais natural desenhar com um lápis e é "
"normalmente melhor para os seus pulsos."

#: ../../user_manual/drawing_tablets.rst:40
msgid ""
"With a properly installed tablet stylus, Krita can use information like "
"pressure sensitivity, allowing you to make strokes that get bigger or "
"smaller depending on the pressure you put on them, to create richer and more "
"interesting strokes."
msgstr ""
"Com um lápis de tablete devidamente instalado, o Krita consegue usar "
"informações como a sensibilidade à pressão, o que lhe permite fazer traços "
"que fiquem maiores ou menores, dependendo da pressão que aplica sobre eles, "
"para criar traços mais ricos e mais interessantes."

#: ../../user_manual/drawing_tablets.rst:46
msgid ""
"Sometimes, people confuse finger-touch styluses with a proper tablet. You "
"can tell the difference because a drawing tablet stylus usually has a pointy "
"nib, while a stylus made for finger-touch has a big rubbery round nib, like "
"a finger. These tablets may not give good results and a pressure-sensitive "
"tablet is recommended."
msgstr ""
"Algumas vezes, as pessoas confundem os lápis com toque do dedo com uma "
"tablete verdadeira. Poderá ver a diferença por o lápis da tablete de desenho "
"normalmente tem um bico pontiagudo, enquanto um lápis feito para toques do "
"dedo tem um bico redondo em borracha, como um dedo. Estas tabletes podem não "
"gerar bons resultados, pelo que se recomenda uma tablete sensível à pressão."

#: ../../user_manual/drawing_tablets.rst:51
msgid "Drivers and Pressure Sensitivity"
msgstr "Controladores e Sensibilidade à Pressão"

#: ../../user_manual/drawing_tablets.rst:53
msgid ""
"So you have bought a tablet, a real drawing tablet. And you wanna get it to "
"work with Krita! So you plug in the USB cable, start up Krita and... It "
"doesn’t work! Or well, you can make strokes, but that pressure sensitivity "
"you heard so much about doesn’t seem to work."
msgstr ""
"Com que então comprou uma tablete, uma verdadeira tablete de desenho. E você "
"quer pô-la a funcionar com o Krita! Assim, pode ligar o cabo USB, iniciar o "
"Krita e... Não funciona! Ou por outra, consegue desenhar traços, mas essa "
"sensibilidade à pressão que ouviu falar não parece estar a funcionar."

#: ../../user_manual/drawing_tablets.rst:58
msgid ""
"This is because you need to install a program called a ‘driver’. Usually you "
"can find the driver on a CD that was delivered alongside your tablet, or on "
"the website of the manufacturer. Go install it, and while you wait, we’ll go "
"into the details of what it is!"
msgstr ""
"Isto acontece porque precisa de instalar um programa chamado de "
"‘controlador’. Normalmente consegue encontrar o controlador num CD que foi "
"fornecido em conjunto com a sua tablete ou na página Web do fabricante. Por "
"favor instale-o e, enquanto espera, podemos ir aos detalhes do que faz!"

#: ../../user_manual/drawing_tablets.rst:63
msgid ""
"Running on your computer is a basic system doing all the tricky bits of "
"running a computer for you. This is the operating system, or OS. Most people "
"use an operating system called Windows, but people on an Apple device have "
"an operating system called MacOS, and some people, including many of the "
"developers use a system called Linux."
msgstr ""
"Existe um sistema básico a correr no seu computador que faz todo o tipo de "
"tarefas necessárias para que ele funcione. Isto é o sistema operativo ou SO. "
"A maioria das pessoas usam um sistema operativo chamado Windows, mas as "
"pessoas com dispositivos da Apple usam um sistema operativo chamado MacOS e "
"ainda algumas pessoas, incluindo muitos dos programadores, usam um sistema "
"chamado Linux."

#: ../../user_manual/drawing_tablets.rst:69
msgid ""
"The base principle of all of these systems is the same though. You would "
"like to run programs like Krita, called software, on your computer, and you "
"want Krita to be able to communicate with the hardware, like your drawing "
"tablet. But to have those two communicate can be really difficult - so the "
"operating system, works as a glue between the two."
msgstr ""
"O princípio de base de todos estes sistemas é o mesmo, contudo. Poderá "
"querer executar programas como o Krita no seu computador, chamados de "
"'software' ou aplicações, e deseja que o Krita consiga comunicar com o "
"'hardware', como a sua tablete de desenho. Mas ter estes dois a comunicar um "
"com o outro pode ser bastante difícil - como tal, o sistema operativo "
"funciona como uma \"cola\" entre os dois."

#: ../../user_manual/drawing_tablets.rst:75
msgid ""
"Whenever you start Krita, Krita will first make connections with the "
"operating system, so it can ask it for a lot of these things: It would like "
"to display things, and use the memory, and so on. Most importantly, it would "
"like to get information from the tablet!"
msgstr ""
"Sempre que iniciar o Krita, o mesmo irá estabelecer ligações ao sistema "
"operativo, para que lhe possa perguntar algumas destas coisas: gostaria de "
"usar determinadas coisas, usar a memória, e assim por diante. O mais "
"importante é que gostaria de obter informações da tablete!"

#: ../../user_manual/drawing_tablets.rst:81
msgid ".. image:: images/Krita_tablet_drivermissing.png"
msgstr ".. image:: images/Krita_tablet_drivermissing.png"

#: ../../user_manual/drawing_tablets.rst:82
msgid ""
"But it can’t! Turns out your operating system doesn’t know much about "
"tablets. That’s what drivers are for. Installing a driver gives the "
"operating system enough information so the OS can provide Krita with the "
"right information about the tablet. The hardware manufacturer's job is to "
"write a proper driver for each operating system."
msgstr ""
"Mas não consegue! O que se passa é que o seu sistema operativo não sabe "
"muito sobre tabletes. É para isso que servem os controladores. A instalação "
"de um controlador dá ao sistema operativo informações suficientes para que o "
"SO consiga passar ao Krita as informações correctas sobre a tablete. A "
"tarefa do fabricante do 'hardware' é criar um controlador adequado para cada "
"sistema operativo."

#: ../../user_manual/drawing_tablets.rst:89
msgid ""
"Because drivers modify the operating system a little, you will always need "
"to restart your computer when installing or deinstalling a driver, so don’t "
"forget to do this! Conversely, because Krita isn’t a driver, you don’t need "
"to even deinstall it to reset the configuration, just rename or delete the "
"configuration file."
msgstr ""
"Como os controladores mudam um pouco o sistema operativo, terá sempre de "
"reiniciar o seu computador quando instalar ou desinstalar um controlador, "
"por isso não se esqueça de o fazer! Do mesmo modo, dado que o Krita não é um "
"controlador, não é preciso desinstalá-lo para repor a configuração; basta "
"mudar o nome ou apagar o ficheiro de configuração."

#: ../../user_manual/drawing_tablets.rst:92
msgid "Where it can go wrong: Windows"
msgstr "Onde pode correr mal: Windows"

#: ../../user_manual/drawing_tablets.rst:94
msgid ""
"Krita automatically connects to your tablet if the drivers are installed. "
"When things go wrong, usually the problem isn't with Krita."
msgstr ""
"O Krita liga-se automaticamente à sua tablete, caso os controladores estejam "
"instalados. Quando as coisas correm mal, normalmente o problema não é do "
"Krita."

#: ../../user_manual/drawing_tablets.rst:98
msgid "Surface pro tablets need two drivers"
msgstr "As tabletes Surface Pro precisam de dois controladores"

#: ../../user_manual/drawing_tablets.rst:100
msgid ""
"Certain tablets using n-trig, like the Surface Pro, have two types of "
"drivers. One is native, n-trig and the other one is called wintab. Since "
"3.3, Krita can use Windows Ink style drivers, just go to :menuselection:"
"`Settings --> Configure Krita --> Tablet Settings` and toggle the :guilabel:"
"`Windows 8+ Pointer Input` there. You don't need to install the wintab "
"drivers anymore for n-trig based pens."
msgstr ""
"Certas tabletes que usam o N-trig, como o Surface Pro, têm dois tipos de "
"controladores. Um é o nativo, o N-Trig, e o outro chama-se Wintab. Desde o "
"3.3, o Krita consegue usar os controladores do tipo Windows Ink; basta ir a :"
"menuselection:`Configuração --> Configurar o Krita --> Configuração da "
"Tablete` e activar aí a opção de :guilabel:`Entrada de cursor do Windows "
"8+`. Não precisa de instalar mais os controladores Wintab para os lápis "
"baseados no N-trig."

#: ../../user_manual/drawing_tablets.rst:108
msgid "Windows 10 updates"
msgstr "Actualizações para o Windows 10"

#: ../../user_manual/drawing_tablets.rst:110
msgid ""
"Sometimes a Windows 10 update can mess up tablet drivers. In that case, "
"reinstalling the drivers should work."
msgstr ""
"Em alguns casos, uma actualização para o Windows 10 pode baralhar os "
"controladores da tablete. Nesse caso, se reinstalar os controladores deverá "
"resultar."

#: ../../user_manual/drawing_tablets.rst:114
msgid "Wacom Tablets"
msgstr "Tabletes da Wacom"

#: ../../user_manual/drawing_tablets.rst:116
msgid "There are two known problems with Wacom tablets and Windows."
msgstr "Existem dois problemas conhecidos com as tabletes Wacom e o Windows."

#: ../../user_manual/drawing_tablets.rst:118
msgid ""
"The first is that if you have customized the driver settings, then "
"sometimes, often after a driver update, but that is not necessary, the "
"driver breaks. Resetting the driver to the default settings and then loading "
"your settings from a backup will solve this problem."
msgstr ""
"A primeira é que, se tiver personalizado as definições do controlador, então "
"algumas vezes, após uma actualização do controlador, o controlador poderá "
"estoirar. Se repor o controlador com os valores de fábrica e depois carregar "
"a sua configuração a partir de uma cópia de segurança, isto irá resolver o "
"problema."

#: ../../user_manual/drawing_tablets.rst:123
msgid ""
"The second is that for some reason it might be necessary to change the "
"display priority order. You might have to make your Cintiq screen your "
"primary screen, or, on the other hand, make it the secondary screen. Double "
"check in the Wacom settings utility that the tablet in the Cintiq is "
"associated with the Cintiq screen."
msgstr ""
"A segunda é que, por alguma razão, poderá ser necessário mudar a ordem de "
"prioridade dos ecrãs. Poderá ter de tornar o seu ecrã Cintiq o seu ecrã "
"principal ou, por outro lado, torná-lo o ecrã secundário. Verifique bem na "
"configuração da Wacom que a tablete no Cintiq está associada ao ecrã Cintiq."

#: ../../user_manual/drawing_tablets.rst:130
msgid "Broken Drivers"
msgstr "Controladores com Problemas"

#: ../../user_manual/drawing_tablets.rst:132
msgid ""
"Tablet drivers need to be made by the manufacturer. Sometimes, with really "
"cheap tablets, the hardware is fine, but the driver is badly written, which "
"means that the driver just doesn’t work well. We cannot do anything about "
"this, sadly. You will have to send a complaint to the manufacturer for this, "
"or buy a better tablet with better quality drivers."
msgstr ""
"Os controladores das tabletes têm de ser criados pelo fabricante. Algumas "
"das vezes, com as tabletes realmente baratas, o 'hardware' é óptimo, mas o "
"controlador foi mal desenvolvido, o que significa que o controlador pode "
"simplesmente não funcionar de todo. Não podemos fazer nada quanto a isto, "
"infelizmente. Terá de emitir uma reclamação ao fabricante por causa disto, "
"ou então comprar uma tablete melhor com controladores de melhor qualidade."

#: ../../user_manual/drawing_tablets.rst:140
msgid "Conflicting Drivers"
msgstr "Controladores em Conflito"

#: ../../user_manual/drawing_tablets.rst:142
msgid ""
"On Windows, you can only have a single wintab-style driver installed at a "
"time. So be sure to deinstall the previous driver before installing the one "
"that comes with the tablet you want to use. Other operating systems are a "
"bit better about this, but even Linux, where the drivers are often "
"preinstalled, can't run two tablets with different drivers at once."
msgstr ""
"No Windows, só pode ter um controlador do estilo Wintab de cada vez. Como "
"tal, certifique-se que desinstala o controlador anterior antes de instalar o "
"novo que vem com o tablete que deseja usar. Os outros sistemas operativos "
"são um pouco melhores sobre isto, mas até o Linux, onde os controladores vêm "
"normalmente pré-instalados, não conseguem correr duas tabletes com "
"controladores diferentes ao mesmo tempo."

#: ../../user_manual/drawing_tablets.rst:150
msgid "Interfering software"
msgstr "'Software' com Interferências"

#: ../../user_manual/drawing_tablets.rst:152
msgid ""
"Sometimes, there's software that tries to make a security layer between "
"Krita and the operating system. Sandboxie is an example of this. However, "
"Krita cannot always connect to certain parts of the operating system while "
"sandboxed, so it will often break in programs like sandboxie. Similarly, "
"certain mouse software, like Razer utilities can also affect whether Krita "
"can talk to the operating system, converting tablet information to mouse "
"information. This type of software should be configured to leave Krita "
"alone, or be deinstalled."
msgstr ""
"Em alguns casos, existem algumas aplicações que tentam criar uma camada de "
"segurança entre o Krita e o sistema operativo. O Sandboxie é um destes "
"exemplos. Contudo, o Krita nem sempre consegue ligar a certas partes do "
"sistema operativo enquanto estiver em segurança, pelo que irá sempre "
"funcionar mal com programas como o Sandboxie. Da forma semelhante, certas "
"aplicações do rato, como os utilitários do Razer, também poderão afectar se "
"o Krita consegue falar com o sistema operativo, convertendo a informação do "
"tablete em informação do rato. Este tipo de aplicações deverá estar "
"configurado para deixar o Krita em paz ou serem desinstaladas."

#: ../../user_manual/drawing_tablets.rst:161
msgid ""
"The following software has been reported to interfere with tablet events to "
"Krita:"
msgstr ""
"As seguintes aplicações foram detectadas como interferindo com os eventos de "
"tablete no Krita:"

#: ../../user_manual/drawing_tablets.rst:164
msgid "Sandboxie"
msgstr "Sandboxie"

#: ../../user_manual/drawing_tablets.rst:165
msgid "Razer mouse utilities"
msgstr "Utilitários do rato Razer"

#: ../../user_manual/drawing_tablets.rst:166
msgid "AMD catalyst “game mode” (this broke the right click for someone)"
msgstr ""
"“Modo de jogo” do AMD Catalyst (isto quebrou o funcionamento do botão "
"direito para algumas pessoas)"

#: ../../user_manual/drawing_tablets.rst:169
msgid "Flicks (Wait circle showing up and then calling the popup palette)"
msgstr "Gestos (Círculo de espera a aparecer e depois a invocar a paleta)"

#: ../../user_manual/drawing_tablets.rst:171
msgid ""
"If you have a situation where trying to draw keeps bringing up the pop-up "
"palette on Windows, then the problem might be flicks. These are a type of "
"gesture, a bit of Windows functionality that allows you to make a motion to "
"serve as a keyboard shortcut. Windows automatically turns these on when you "
"install tablet drivers, because the people who made this part of Windows "
"forgot that people also draw with computers. So you will need to turn it off "
"in the Windows flicks configuration."
msgstr ""
"Se tiver uma situação em que, ao tentar desenhar, estiver sempre a mostrar a "
"paleta no Windows, então o problema pode ser com os gestos. Isso corresponde "
"a um tipo de gestos e uma funcionalidade do Windows que permite-lhe fazer "
"com que um movimento sirva como atalho de teclado. O Windows activa-os "
"automaticamente quando instala os controladores da tablete, porque as "
"pessoas que tornaram isto parte do Windows, esqueceram-se que as pessoas "
"também desenham com computadores. Por isso, terá de desligar isto na "
"configuração de gestos do Windows."

#: ../../user_manual/drawing_tablets.rst:180
msgid "Wacom Double Click Sensitivity (Straight starts of lines)"
msgstr "Sensibilidade do Duplo-Click da Wacom (Início de linhas rectas)"

#: ../../user_manual/drawing_tablets.rst:182
msgid ""
"If you experience an issue where the start of the stroke is straight, and "
"have a wacom tablet, it could be caused by the Wacom driver double-click "
"detection."
msgstr ""
"Se tiver algum problema em que o início do traço é a direito e tiver uma "
"tablete Wacom, poderá ser devido à detecção de duplo-click do controlador "
"Wacom."

#: ../../user_manual/drawing_tablets.rst:186
msgid ""
"To fix this, go to the Wacom settings utility and lower the double click "
"sensitivity."
msgstr ""
"Para corrigir isto, vá ao utilitário de configuração do Wacom e baixe a "
"sensibilidade do duplo-click."

#: ../../user_manual/drawing_tablets.rst:190
msgid "Supported Tablets"
msgstr "Tabletes Suportadas"

#: ../../user_manual/drawing_tablets.rst:192
msgid ""
"Supported tablets are the ones of which Krita developers have a version "
"themselves, so they can reliably fix bugs with them. :ref:`We maintain a "
"list of those here <list_supported_tablets>`."
msgstr ""
"As tabletes suportadas são as que os próprios programadores do Krita usam, "
"por isso conseguem corrigir os problemas com elas. :ref:`É mantida uma lista "
"com elas aqui <list_supported_tablets>`."
