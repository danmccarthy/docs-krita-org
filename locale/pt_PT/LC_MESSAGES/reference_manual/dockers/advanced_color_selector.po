# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 10:36+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en MyPaint menuselection jpg images Rec Hue\n"
"X-POFile-SpellExtra: Saturation HSI HSL Advancecolorselector HSX HSY\n"
"X-POFile-SpellExtra: Lightness Coefficients Luma Value RFB kbd Krita\n"
"X-POFile-SpellExtra: KritaColorSelectorTypes Intensity sRGB image dockers\n"
"X-POFile-SpellExtra: ref gamutmaskdocker\n"

#: ../../reference_manual/dockers/advanced_color_selector.rst:1
msgid "Overview of the advanced color selector docker."
msgstr "Introdução à área de selecção avançada de cores."

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
#: ../../reference_manual/dockers/advanced_color_selector.rst:16
msgid "Advanced Color Selector"
msgstr "Selector de Cores Avançado"

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
#: ../../reference_manual/dockers/advanced_color_selector.rst:24
msgid "Color Selector"
msgstr "Selecção de Cores"

#: ../../reference_manual/dockers/advanced_color_selector.rst:11
msgid "Color"
msgstr "Cor"

#: ../../reference_manual/dockers/advanced_color_selector.rst:20
msgid ".. image:: images/dockers/Advancecolorselector.jpg"
msgstr ".. image:: images/dockers/Advancecolorselector.jpg"

#: ../../reference_manual/dockers/advanced_color_selector.rst:21
msgid ""
"As compared to other color selectors in Krita, Advanced color selector "
"provides more control and options to the user. To open Advanced color "
"selector choose :menuselection:`Settings --> Dockers --> Advanced Color "
"Selector`. You can configure this docker by clicking on the little wrench "
"icon on the top left corner. Clicking on the wrench will open a popup window "
"with following tabs and options:"
msgstr ""
"Em comparação com os outros selectores de cores no Krita, o selector de "
"cores avançadas oferece mais controlo e opções ao utilizador. Para abrir o "
"selector de cores avançado, poderá escolher :menuselection:`Configuração --> "
"Áreas Acopláveis --> Selector de Cores Avançado`. Poderá configurar esta "
"área acoplável se carregar no pequeno ícone de engrenagem no canto superior "
"esquerdo. Ao fazê-lo, irá aparecer uma janela instantaneamente com as "
"seguintes páginas e opções :"

#: ../../reference_manual/dockers/advanced_color_selector.rst:26
msgid "Here you configure the main selector."
msgstr "Aqui poderá configurar o selector principal."

#: ../../reference_manual/dockers/advanced_color_selector.rst:28
msgid "Show Color Selector"
msgstr "Mostrar o Selector de Cores"

#: ../../reference_manual/dockers/advanced_color_selector.rst:32
msgid ""
"This allows you to configure whether to show or hide the main color selector."
msgstr ""
"Isto permite-lhe configurar se deve mostrar ou esconder o selector de cores "
"principal."

#: ../../reference_manual/dockers/advanced_color_selector.rst:35
msgid "Type and Shape"
msgstr "Tipo e Forma"

#: ../../reference_manual/dockers/advanced_color_selector.rst:38
msgid ".. image:: images/dockers/Krita_Color_Selector_Types.png"
msgstr ".. image:: images/dockers/Krita_Color_Selector_Types.png"

#: ../../reference_manual/dockers/advanced_color_selector.rst:39
msgid ""
"Here you can pick the hsx model you'll be using. There's a small blurb "
"explaining the characteristic of each model, but let's go into detail:"
msgstr ""
"Aqui poderá escolher o modelo HSX que irá usar. Existe um pequeno texto que "
"explica a característica de cada modelo, mas iremos detalhar mais um pouco:"

#: ../../reference_manual/dockers/advanced_color_selector.rst:42
msgid "HSV"
msgstr "HSV"

#: ../../reference_manual/dockers/advanced_color_selector.rst:43
msgid ""
"Stands for Hue, Saturation, Value. Saturation determines the difference "
"between white, gray, black and the most colorful color. Value in turn "
"measures either the difference between black and white, or the difference "
"between black and the most colorful color."
msgstr ""
"Significa Hue (Tom/Matiz), Saturation (Saturação) e Value (Valor). A "
"saturação define a diferença entre o branco, o cinzento, o preto e as cores "
"mais intensas. O valor, por sua vez, mede a diferença entre o preto e o "
"branco ou a diferença entre o preto e a cor mais intensa."

#: ../../reference_manual/dockers/advanced_color_selector.rst:44
msgid "HSL"
msgstr "HSL"

#: ../../reference_manual/dockers/advanced_color_selector.rst:45
msgid ""
"Stands for Hue, Saturation, Lightness. All saturated colors are equal to 50% "
"lightness. Saturation allows for shifting between gray and color."
msgstr ""
"Significa Hue (Tom/Matiz), Saturation (Saturação) e Lightness "
"(Luminosidade). Todas as cores saturadas são iguais a 50% de luminosidade. A "
"saturação permite desviar entre o cinzento e uma dada cor."

#: ../../reference_manual/dockers/advanced_color_selector.rst:46
msgid "HSI"
msgstr "HSI"

#: ../../reference_manual/dockers/advanced_color_selector.rst:47
msgid ""
"This stands for Hue, Saturation and Intensity. Unlike HSL, this one "
"determine the intensity as the sum of total rgb components. Yellow (1,1,0) "
"has higher intensity than blue (0,0,1) but is the same intensity as cyan "
"(0,1,1)."
msgstr ""
"Significa Hue (Tom/Matiz), Saturation (Saturação) e Intensity (Intensidade). "
"Ao contrário do HSL, este define a intensidade como a soma de todas as "
"componentes do RFB. O Amarelo (1, 1, 0) tem maior intensidade que o Azul (0, "
"0, 1), mas tem a mesma intensidade que o Cíano (0, 1, 1)."

#: ../../reference_manual/dockers/advanced_color_selector.rst:49
msgid "HSY'"
msgstr "HSY'"

#: ../../reference_manual/dockers/advanced_color_selector.rst:49
msgid ""
"Stands for Hue, Saturation, Luma, with Luma being an RGB approximation of "
"true luminosity. (Luminosity being the measurement of relative lightness). "
"HSY' uses the Luma Coefficients, like `Rec. 709 <https://en.wikipedia.org/"
"wiki/Rec._709>`_, to calculate the Luma. Due to this, HSY' can be the most "
"intuitive selector to work with, or the most confusing."
msgstr ""
"Significa Hue (Tom/Matiz), Saturation (Saturação) e Luma, sendo o Luma uma "
"aproximação em RGB da luminosidade real (A Luminosidade é a medida da "
"luminosidade relativa). O HSY' usa os Coeficientes de Luma Coefficients, "
"como a `Rec 709 <https://en.wikipedia.org/wiki/Rec._709>`_, para calcular o "
"valor do Luma. Por esse facto, o HSY' pode ser o selector mais intuitivo com "
"o qual lidar ou o mais confuso."

#: ../../reference_manual/dockers/advanced_color_selector.rst:51
msgid ""
"Then, under shape, you can select one of the shapes available within that "
"color model."
msgstr ""
"Como tal, sob a forma, poderá seleccionar uma das formas disponíveis dentro "
"desse modelo de cores."

#: ../../reference_manual/dockers/advanced_color_selector.rst:55
msgid ""
"Triangle is in all color models because to a certain extent, it is a "
"wildcard shape: All color models look the same in an equilateral triangle "
"selector."
msgstr ""
"O triângulo existe em todos os modelos de cores porque, a certo nível, é uma "
"forma multiusos: Todos os modelos de cores parecem iguais num selector com "
"um triângulo equilátero."

#: ../../reference_manual/dockers/advanced_color_selector.rst:58
msgid "Luma Coefficients"
msgstr "Coeficientes de Luma"

#: ../../reference_manual/dockers/advanced_color_selector.rst:60
msgid ""
"This allows you to edit the Luma coefficients for the HSY model selectors to "
"your leisure. Want to use `Rec. 601 <https://en.wikipedia.org/wiki/Rec."
"_601>`_ instead of Rec. 709? These boxes allow you to do that!"
msgstr ""
"Isto permite-lhe editar os coeficientes de Luma para os selectores do modelo "
"HSY a seu gosto. Gostaria de usar a `Rec 601 <https://en.wikipedia.org/wiki/"
"Rec._601>`_ em vez da Rec 709? Estes campos permitem-lhe fazer isso mesmo!"

#: ../../reference_manual/dockers/advanced_color_selector.rst:62
msgid "By default, the Luma coefficients should add up to 1 at maximum."
msgstr ""
"Por omissão, os coeficientes de Luma deverão ser somados até um valor máximo "
"igual a 1."

#: ../../reference_manual/dockers/advanced_color_selector.rst:65
msgid "Gamma"
msgstr "Gama"

#: ../../reference_manual/dockers/advanced_color_selector.rst:65
msgid ""
"The HSY selector is linearised, this setting allows you to choose how much "
"gamma is applied to the Luminosity for the gui element. 1.0 is fully linear, "
"2.2 is the default."
msgstr ""
"O selector HSY é linearizado; esta opção permite-lhe escolher que valor do "
"'gama' é aplicado à Luminosidade para o elemento gráfico. O valor 1,0 é "
"completamente linear; o valor 2,2 é o valor por omissão."

#: ../../reference_manual/dockers/advanced_color_selector.rst:68
msgid "Color Space"
msgstr "Espaço de Cores"

#: ../../reference_manual/dockers/advanced_color_selector.rst:70
msgid ""
"This allows you to set the overall color space for the Advanced Color "
"Selector."
msgstr ""
"Isto permite-lhe definir o espaço de cores global para o Selector de Cores "
"Avançadas."

#: ../../reference_manual/dockers/advanced_color_selector.rst:73
msgid ""
"You can pick only sRGB colors in advanced color selector regardless of the "
"color space of advanced color selector. This is a bug."
msgstr ""
"Poderá escolher apenas cores do sRGB no selector avançado, independentemente "
"do espaço de cores actual. Isto é um erro."

#: ../../reference_manual/dockers/advanced_color_selector.rst:76
msgid "Behavior"
msgstr "Comportamento"

#: ../../reference_manual/dockers/advanced_color_selector.rst:79
msgid "When docker resizes"
msgstr "Quando a área muda de tamanho"

#: ../../reference_manual/dockers/advanced_color_selector.rst:81
msgid "This determines the behavior of the widget as it becomes smaller."
msgstr ""
"Isto define o comportamento do elemento gráfico à medida que fica menor."

#: ../../reference_manual/dockers/advanced_color_selector.rst:83
msgid "Change to Horizontal"
msgstr "Mudar para Horizontal"

#: ../../reference_manual/dockers/advanced_color_selector.rst:84
msgid ""
"This'll arrange the shade selector horizontal to the main selector. Only "
"works with the MyPaint shade selector."
msgstr ""
"Isto irá organizar o selector de sombras num formato horizontal do selector "
"principal. Só funciona com o selector de sombras do MyPaint."

#: ../../reference_manual/dockers/advanced_color_selector.rst:85
msgid "Hide Shade Selector."
msgstr "Esconder o Selector de Sombras."

#: ../../reference_manual/dockers/advanced_color_selector.rst:86
msgid "This hides the shade selector."
msgstr "Isto esconde o selector de sombras."

#: ../../reference_manual/dockers/advanced_color_selector.rst:88
msgid "Do nothing"
msgstr "Não fazer nada"

#: ../../reference_manual/dockers/advanced_color_selector.rst:88
msgid "Does nothing, just resizes."
msgstr "Não faz nada; apenas ajusta o tamanho."

#: ../../reference_manual/dockers/advanced_color_selector.rst:91
msgid "Zoom selector UI"
msgstr "Interface de selecção da ampliação"

#: ../../reference_manual/dockers/advanced_color_selector.rst:93
msgid ""
"If your have set the docker size considerably smaller to save space, this "
"option might be helpful to you. This allows you to set whether or not the "
"selector will give a zoomed view of the selector in a size specified by you, "
"you have these options for the zoom selector:"
msgstr ""
"Se tiver definido o tamanho da área acoplável de uma forma consideravelmente "
"menor para poupar espaço, esta opção poderá ser útil para si. Isto permite-"
"lhe definir se o selector irá dar uma visão ampliada do selector para um "
"tamanho indicado por si; terá essas opções no selector de ampliação:"

#: ../../reference_manual/dockers/advanced_color_selector.rst:95
msgid "when pressing middle mouse button"
msgstr "ao carregar com o botão do meio do rato"

#: ../../reference_manual/dockers/advanced_color_selector.rst:96
msgid "on mouse over"
msgstr "à passagem do rato"

#: ../../reference_manual/dockers/advanced_color_selector.rst:97
msgid "never"
msgstr "nunca"

#: ../../reference_manual/dockers/advanced_color_selector.rst:99
msgid ""
"The size given here, is also the size of the Main Color Selector and the "
"MyPaint Shade Selector when they are called with the :kbd:`Shift + I` and :"
"kbd:`Shift + M` shortcuts, respectively."
msgstr ""
"O tamanho aqui indicado é também o tamanho do Selector de Cores Principal e "
"do Selector de Sombras do MyPaint, quando forem invocados com o :kbd:`Shift "
"+ I` e o :kbd:`Shift + M`, respectivamente."

#: ../../reference_manual/dockers/advanced_color_selector.rst:102
msgid "Hide Pop-up on click"
msgstr "Esconder a mensagem ao carregar"

#: ../../reference_manual/dockers/advanced_color_selector.rst:102
msgid ""
"This allows you to let the pop-up selectors called with the above hotkeys to "
"disappear upon clicking them instead of having to leave the pop-up boundary. "
"This is useful for faster working."
msgstr ""
"Isto permite-lhe invocar os selectores imediatamente com as combinações de "
"teclas acima para os fazer desaparecer, em vez de ter de carregar fora dos "
"limites da área. Isto é útil para trabalhar mais depressa."

#: ../../reference_manual/dockers/advanced_color_selector.rst:105
msgid "Shade selector"
msgstr "Selector de sombras"

#: ../../reference_manual/dockers/advanced_color_selector.rst:107
msgid ""
"Shade selector options. The shade selectors are useful to decide upon new "
"shades of color."
msgstr ""
"Opções do selector de sombras. Os selectores de sombras são úteis para "
"decidir novas tonalidades da cor."

#: ../../reference_manual/dockers/advanced_color_selector.rst:111
msgid "Update Selector"
msgstr "Actualizar o Selector"

#: ../../reference_manual/dockers/advanced_color_selector.rst:113
msgid "This allows you to determine when the shade selector updates."
msgstr "Isto permite-lhe definir quando é actualizado o selector de sombras."

#: ../../reference_manual/dockers/advanced_color_selector.rst:116
msgid "MyPaint Shade Selector"
msgstr "Selector de Sombras do MyPaint"

#: ../../reference_manual/dockers/advanced_color_selector.rst:118
msgid ""
"Ported from MyPaint, and extended with all color models. Default hotkey is :"
"kbd:`Shift + M`."
msgstr ""
"Migrado a partir do MyPaint e alargado com todos os modelos de cores. O "
"valor por omissão é o :kbd:`Shift + M`."

#: ../../reference_manual/dockers/advanced_color_selector.rst:122
msgid "Simple Shade Selector"
msgstr "Selector de Sombras Simples"

#: ../../reference_manual/dockers/advanced_color_selector.rst:124
msgid "This allows you to configure the simple shade selector in detail."
msgstr ""
"Isto permite-lhe configurar o selector de sombras simples com mais detalhe."

#: ../../reference_manual/dockers/advanced_color_selector.rst:127
msgid "Color Patches"
msgstr "Padrões de Cores"

#: ../../reference_manual/dockers/advanced_color_selector.rst:129
msgid "This sets the options of the color patches."
msgstr "Isto configura as opções dos padrões de cores."

#: ../../reference_manual/dockers/advanced_color_selector.rst:131
msgid ""
"Both Color History and Colors From the Image have similar options which will "
"be explained below."
msgstr ""
"Tanto o Histórico de Cores como as Cores da Imagem têm opções semelhantes "
"que serão explicadas em baixo com mais detalhe."

#: ../../reference_manual/dockers/advanced_color_selector.rst:133
msgid "Show"
msgstr "Mostrar"

#: ../../reference_manual/dockers/advanced_color_selector.rst:134
msgid ""
"This is a radio button to show or hide the section. It also determines "
"whether or not the colors are visible with the advanced color selector "
"docker."
msgstr ""
"Isto é um botão exclusivo para mostrar ou esconder a secção. Também "
"determina quando é que as cores estão visíveis ou não com a área do selector "
"de cores avançadas."

#: ../../reference_manual/dockers/advanced_color_selector.rst:135
msgid "Size"
msgstr "Tamanho"

#: ../../reference_manual/dockers/advanced_color_selector.rst:136
msgid "The size of the color boxes can be set here."
msgstr "Poderá definir o tamanho das caixas de cores."

#: ../../reference_manual/dockers/advanced_color_selector.rst:137
msgid "Patch Count"
msgstr "Número de Padrões"

#: ../../reference_manual/dockers/advanced_color_selector.rst:138
msgid "The number of patches to display."
msgstr "O número de padrões a apresentar."

#: ../../reference_manual/dockers/advanced_color_selector.rst:139
msgid "Direction"
msgstr "Direcção"

#: ../../reference_manual/dockers/advanced_color_selector.rst:140
msgid "The direction of the patches, Horizontal or Vertical."
msgstr "A direcção dos padrões, sendo na Horizontal ou na Vertical."

#: ../../reference_manual/dockers/advanced_color_selector.rst:141
msgid "Allow Scrolling"
msgstr "Permitir o Deslocamento"

#: ../../reference_manual/dockers/advanced_color_selector.rst:142
msgid ""
"Whether to allow scrolling in the section or not when there are too many "
"patches."
msgstr ""
"Se deve permitir o deslocamento na secção ou não, quando existem demasiados "
"padrões."

#: ../../reference_manual/dockers/advanced_color_selector.rst:143
msgid "Number of Columns/Rows"
msgstr "Número de Colunas/Linhas"

#: ../../reference_manual/dockers/advanced_color_selector.rst:144
msgid "The number of Columns or Rows to show in the section."
msgstr "O número de Colunas ou Linhas a apresentar na secção."

#: ../../reference_manual/dockers/advanced_color_selector.rst:146
msgid "Update After Every Stroke"
msgstr "Actualizar a Cada Traço"

#: ../../reference_manual/dockers/advanced_color_selector.rst:146
msgid ""
"This is only available for Colors From the Image and tells the docker "
"whether to update the section after every stroke or not, as after each "
"stroke the colors will change in the image."
msgstr ""
"Isto só fica disponível para as Cores da Imagem e diz à área acoplável se "
"deve actualizar a secção a cada novo traço ou não, já que a cada traço as "
"cores irão mudar na imagem."

#: ../../reference_manual/dockers/advanced_color_selector.rst:149
msgid "History patches"
msgstr "Histórico de padrões"

#: ../../reference_manual/dockers/advanced_color_selector.rst:151
msgid ""
"The history patches remember which colors you've drawn on canvas with. They "
"can be quickly called with the :kbd:`H` key."
msgstr ""
"Os padrões do histórico recordam as cores com que desenhou na área de "
"desenho. Poderá invocar o histórico rapidamente com o :kbd:`H`."

#: ../../reference_manual/dockers/advanced_color_selector.rst:154
msgid "Common Patches"
msgstr "Padrões Comuns"

#: ../../reference_manual/dockers/advanced_color_selector.rst:156
msgid ""
"The common patches are generated from the image, and are the most common "
"color in the image. The hotkey for them on canvas is the :kbd:`U` key."
msgstr ""
"Os padrões comuns são gerados a partir da imagem, sendo as cores mais comuns "
"na imagem. A combinação de teclas para eles na área de desenho é a :kbd:`U`."

#: ../../reference_manual/dockers/advanced_color_selector.rst:159
msgid "Gamut masking"
msgstr "Máscara do gamute"

#: ../../reference_manual/dockers/advanced_color_selector.rst:165
msgid ""
"Gamut masking is available only when the selector shape is set to wheel."
msgstr ""
"A máscara do gamute só está disponível quando a forma do selector for a roda."

#: ../../reference_manual/dockers/advanced_color_selector.rst:167
msgid ""
"You can select and manage your gamut masks in the :ref:`gamut_mask_docker`."
msgstr ""
"Poderá seleccionar e gerir as suas máscaras de gamute no :ref:"
"`gamut_mask_docker`."

#: ../../reference_manual/dockers/advanced_color_selector.rst:169
msgid ""
"In the gamut masking toolbar at the top of the selector you can toggle the "
"selected mask off and on (left button). You can also rotate the mask with "
"the rotation slider (right)."
msgstr ""
"Na barra de máscara do gamute, no topo do selector, poderá activar e "
"desactivar a máscara seleccionada (botão esquerdo). Também poderá rodar a "
"máscara com a barra de rotação (direita)."

#: ../../reference_manual/dockers/advanced_color_selector.rst:174
msgid "External Info"
msgstr "Informação Externa"

#: ../../reference_manual/dockers/advanced_color_selector.rst:176
msgid ""
"`HSI and HSY for Krita’s advanced color selector. <https://wolthera.info/?"
"p=726>`_"
msgstr ""
"`HSI e HSY para o selector de cores avançadas do Krita. <https://wolthera."
"info/?p=726>`_"
