# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-18 13:04+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Krita guilabel image Kritafiltergradientmap Phong\n"
"X-POFile-SpellExtra: normals images brushes filters\n"

#: ../../reference_manual/filters/map.rst:1
msgid "Overview of the map filters."
msgstr "Introdução aos filtros por mapa."

#: ../../reference_manual/filters/map.rst:11
msgid "Filters"
msgstr "Filtros"

#: ../../reference_manual/filters/map.rst:16
msgid "Map"
msgstr "Mapa"

#: ../../reference_manual/filters/map.rst:18
msgid "Filters that are signified by them mapping the input image."
msgstr ""
"Os filtros cuja significância está associada (mapeada) na imagem de entrada."

#: ../../reference_manual/filters/map.rst:20
#: ../../reference_manual/filters/map.rst:23
msgid "Small Tiles"
msgstr "Padrões Pequenos"

#: ../../reference_manual/filters/map.rst:20
msgid "Tiles"
msgstr "Peças"

#: ../../reference_manual/filters/map.rst:25
msgid "Tiles the input image, using its own layer as output."
msgstr ""
"Cria padrões da imagem de entrada, usando a sua própria camada como "
"resultado."

#: ../../reference_manual/filters/map.rst:27
msgid "Height Map"
msgstr "Mapa de Altura"

#: ../../reference_manual/filters/map.rst:27
msgid "Bumpmap"
msgstr "Mapa de Relevo"

#: ../../reference_manual/filters/map.rst:27
msgid "Normal Map"
msgstr "Mapa da Normal"

#: ../../reference_manual/filters/map.rst:30
msgid "Phong Bumpmap"
msgstr "Mapa de Relevo Phong"

#: ../../reference_manual/filters/map.rst:33
msgid ".. image:: images/brushes/Krita-normals-tutoria_4.png"
msgstr ".. image:: images/brushes/Krita-normals-tutoria_4.png"

#: ../../reference_manual/filters/map.rst:34
msgid ""
"Uses the input image as a height-map to output a 3d something, using the "
"phong-lambert shading model. Useful for checking one's height maps during "
"game texturing. Checking the :guilabel:`Normal Map` box will make it use all "
"channels and interpret them as a normal map."
msgstr ""
"Usa a imagem de entrada como um mapa de altura para gerar uma imagem em 3D "
"de algo, usando o modelo de sombreados Phong-Lambert. Útil para verificar os "
"mapas de altura de alguns objectos durante a gestão de texturas de um jogo. "
"Se assinalar a opção :guilabel:`Mapa da Normal`, fará com que ele use todos "
"os canais e os interprete como um mapa da normal."

#: ../../reference_manual/filters/map.rst:37
msgid "Round Corners"
msgstr "Cantos Arredondados"

#: ../../reference_manual/filters/map.rst:39
msgid "Adds little corners to the input image."
msgstr "Adiciona pequenos cantos à imagem de entrada."

#: ../../reference_manual/filters/map.rst:41
#: ../../reference_manual/filters/map.rst:44
msgid "Normalize"
msgstr "Normalizar"

#: ../../reference_manual/filters/map.rst:46
msgid ""
"This filter takes the input pixels, puts them into a 3d vector, and then "
"normalizes (makes the vector size exactly 1) the values. This is helpful for "
"normal maps and some minor image-editing functions."
msgstr ""
"Este filtro recebe os pixels de entrada, coloca-os num vector 3D e depois "
"normaliza (torna o tamanho do vector exactamente igual a 1) os seus valores. "
"Isto é útil para os mapas da normal e para algumas pequenas funções de "
"edição da imagem."

#: ../../reference_manual/filters/map.rst:48
#: ../../reference_manual/filters/map.rst:51
msgid "Gradient Map"
msgstr "Mapa do Gradiente"

#: ../../reference_manual/filters/map.rst:48
msgid "Gradient"
msgstr "Gradiente"

#: ../../reference_manual/filters/map.rst:54
msgid ".. image:: images/filters/Krita_filter_gradient_map.png"
msgstr ".. image:: images/filters/Krita_filter_gradient_map.png"

#: ../../reference_manual/filters/map.rst:55
msgid ""
"Maps the lightness of the input to the selected gradient. Useful for fancy "
"artistic effects."
msgstr ""
"Associa a luminosidade dos dados de entrada ao gradiente seleccionado. Útil "
"para criar alguns efeitos artísticos."

#: ../../reference_manual/filters/map.rst:57
msgid ""
"In 3.x you could only select predefined gradients. In 4.0, you can select "
"gradients and change them on the fly, as well as use the gradient map filter "
"as a filter layer or filter brush."
msgstr ""
"No 3.x, poderia seleccionar apenas os gradientes predefinidos. No 4.0, "
"poderá seleccionar os gradientes e alterá-los na hora, assim como usar o "
"filtro do mapa do gradiente como uma camada de filtragem ou pincel de "
"filtragem."
