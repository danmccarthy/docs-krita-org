# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-24 09:20+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: miscsettings Krita menuselection kws kra ref\n"

#: ../../reference_manual/resource_management/resource_workspace.rst:1
msgid "Managing workspaces and sessions in Krita."
msgstr "Gerir os espaços de trabalho e as sessões no Krita."

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:16
msgid "Workspaces"
msgstr "Espaços de Trabalho"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:25
msgid "Window Layouts"
msgstr "Disposições das Janelas"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
#: ../../reference_manual/resource_management/resource_workspace.rst:37
msgid "Sessions"
msgstr "Sessões"

#: ../../reference_manual/resource_management/resource_workspace.rst:11
msgid "Resources"
msgstr "Recursos"

#: ../../reference_manual/resource_management/resource_workspace.rst:18
msgid ""
"Workspaces are basically saved configurations of dockers.  Each workspace "
"saves how the dockers are grouped and where they are placed on the screen.  "
"They allow you to easily move between workflows without having to manual "
"reconfigure your setup each time.  They can be as simple or as complex as "
"you want."
msgstr ""
"Os espaços de trabalho são basicamente configurações gravadas das áreas "
"acopláveis. Cada espaço de trabalho guarda como estão agrupadas as áreas e "
"onde estão colocadas no ecrã. Eles permitem-lhe percorre facilmente os "
"fluxos de trabalho sem ter de reconfigurar manualmente o seu ambiente em "
"cada altura. Podem ser tão simples ou tão complexas como desejar."

#: ../../reference_manual/resource_management/resource_workspace.rst:20
msgid ""
"Workspaces can only be accessed via the toolbar or :menuselection:`Window --"
"> Workspaces` There's no docker for them.  You can save workspaces, in which "
"your current configuration is saved. You can also import them (from a \\*."
"kws file), or delete them (which black lists them)."
msgstr ""
"Os espaços de trabalho só podem ser acedidos através da barra de ferramentas "
"ou da opção :menuselection:`Janela --> Espaços de Trabalho`. Não existe "
"nenhuma área acoplável para eles. Poderá gravar os espaços de trabalho, nos "
"quais será registada a sua configuração actual. Também os poderá importar (a "
"partir de um ficheiro \\*.kws) ou apagá-los (o que os irá excluir)."

#: ../../reference_manual/resource_management/resource_workspace.rst:22
msgid ""
"Workspaces can technically be tagged, but outside of the resource manager "
"this is not possible."
msgstr ""
"Os espaços de trabalho podem estar marcados a nível técnico, mas fora do "
"gestor de recursos isto não é possível."

#: ../../reference_manual/resource_management/resource_workspace.rst:27
msgid ""
"When you work with multiple screens, a single window with a single workspace "
"won't be enough. For multi monitor setups we instead can use sessions. "
"Window layouts allow us to store multiple windows, their positions and the "
"monitor they were on."
msgstr ""
"Quando lidar com vários ecrãs, uma única janela com um único espaço de "
"trabalho não será suficiente. Para as configurações com vários monitores, "
"poderá usar como alternativa as sessões. As disposições das janelas permitem-"
"nos gravar várias janelas, as suas posições e o monitor onde se encontravam."

#: ../../reference_manual/resource_management/resource_workspace.rst:29
msgid ""
"You can access Window Layouts from the workspace drop-down in the toolbar."
msgstr ""
"Poderá aceder às Disposições de Janelas a partir da lista de espaços de "
"trabalho na barra de ferramentas."

#: ../../reference_manual/resource_management/resource_workspace.rst:31
msgid "Primary Workspace Follows Focus"
msgstr "O Espaço de Trabalho Primário Segue o Foco"

#: ../../reference_manual/resource_management/resource_workspace.rst:32
msgid ""
"This treats the workspace in the first window as the 'primary' workspace, "
"and when you switch focus, it will switch the secondary windows to that "
"primary workspace. This is useful when the secondary workspace is a very "
"sparse workspace with few dockers, and the primary is one with a lot of "
"different dockers."
msgstr ""
"Isto trata o espaço de trabalho na primeira janela como o espaço de trabalho "
"'primário' ou 'principal' e, quando mudar o foco, irá mudar as janelas "
"secundárias para esse espaço de trabalho primário. Isto é útil quando o "
"espaço de trabalho secundário for um espaço muito esparso com poucas áreas "
"acopláveis e o primário é um que tiver várias áreas acopláveis diferentes."

#: ../../reference_manual/resource_management/resource_workspace.rst:34
msgid "Show Active Image In All Windows"
msgstr "Mostrar a Imagem Activa em Todas as Janelas"

#: ../../reference_manual/resource_management/resource_workspace.rst:34
msgid ""
"This will synchronise the currently viewed image in all windows. Without it, "
"different windows can open separate views for an image via :menuselection:"
"`Window --> New View --> document.kra`."
msgstr ""
"Isto irá sincronizar a imagem actualmente em visualização em todas as "
"janelas. Sem ela, as diferentes janelas poderão abrir áreas separadas para "
"uma imagem através da opção :menuselection:`Janela --> Nova Janela --> "
"documento.kra`."

#: ../../reference_manual/resource_management/resource_workspace.rst:39
msgid ""
"Sessions allow Krita to store the images and windows opened. You can tell "
"Krita to automatically save current or recover previous sessions if so "
"configured in the :ref:`misc_settings`."
msgstr ""
"As sessões permitem ao Krita guardar as imagens e janelas abertas. Poderá "
"indicar ao Krita para gravar automaticamente as sessões ou recuperar as "
"anteriores, se for configurado para tal na :ref:`misc_settings`."

#: ../../reference_manual/resource_management/resource_workspace.rst:41
msgid "You can access sessions from :menuselection:`File --> Sessions`."
msgstr "Poderá aceder às sessões em :menuselection:`Ficheiro --> Sessões`."
