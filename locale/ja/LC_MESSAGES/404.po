# Fumiaki Okushi <fumiaki.okushi@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../404.rst:None
msgid ""
".. image:: images/color_category/Kiki_cLUTprofiles.png\n"
"   :alt: Image of Kiki looking confused through books."
msgstr ""

#: ../../404.rst:5
msgid "File Not Found (404)"
msgstr ""

#: ../../404.rst:10
msgid "This page does not exist."
msgstr ""

#: ../../404.rst:12
msgid "This might be because of the following:"
msgstr ""

#: ../../404.rst:14
msgid ""
"We moved the manual from MediaWiki to Sphinx and with that came a lot of "
"reorganization."
msgstr ""

#: ../../404.rst:15
msgid "The page has been deprecated."
msgstr ""

#: ../../404.rst:16
msgid "Or a simple typo in the url."
msgstr ""

#: ../../404.rst:18
msgid ""
"In all cases, you might be able to find the page you're looking for by using "
"the search in the lefthand navigation."
msgstr ""
