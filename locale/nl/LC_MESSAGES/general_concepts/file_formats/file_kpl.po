# Dutch translations for Krita Manual package
# Nederlandse vertalingen voor het pakket Krita Manual.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Freek de Kruijf <freekdekruijf@kde.nl>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 10:59+0200\n"
"Last-Translator: Freek de Kruijf <freekdekruijf@kde.nl>\n"
"Language-Team: Dutch <kde-i18n-nl@kde.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.2\n"

#: ../../general_concepts/file_formats/file_kpl.rst:1
msgid "The Krita Palette file format."
msgstr "Het bestandsformaat Krita Palette."

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "*.kpl"
msgstr "*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "KPL"
msgstr "KPL"

#: ../../general_concepts/file_formats/file_kpl.rst:10
msgid "Krita Palette"
msgstr "Krita Palette"

#: ../../general_concepts/file_formats/file_kpl.rst:15
msgid "\\*.kpl"
msgstr "\\*.kpl"

#: ../../general_concepts/file_formats/file_kpl.rst:17
msgid ""
"Since 4.0, Krita has a new palette file-format that can handle colors that "
"are wide gamut, RGB, CMYK, XYZ, GRAY, or LAB, and can be of any of the "
"available bitdepths, as well as groups. These are Krita Palettes, or ``*."
"kpl``."
msgstr ""
"Sinds 4.0 heeft Krita een nieuw paletbestandsformaat dat kleuren kan "
"behandelen die een breed gamut hebben, RGB, CMYK, XYZ, GRAY of LAB, en elk "
"van de beschikbare bitdiepten, evenals als groepen. Dit zijn Krita Palettes "
"of ``*.kpl``."

#: ../../general_concepts/file_formats/file_kpl.rst:19
msgid ""
"``*.kpl`` files are ZIP files, with two XMLs and ICC profiles inside. The "
"colorset XML contains the swatches as ColorSetEntry and Groups as Group. The "
"profiles.XML contains a list of profiles, and the ICC profiles themselves "
"are embedded to ensure compatibility over different computers."
msgstr ""
"``*.kpl`` bestanden zijn ZIP-bestanden met erin twee XML- en ICC-profielen. "
"De kleurset XML bevat de swatches als ColorSetEntry en Groups als Group. De "
"profiles.XML bevatten een lijst met profielen en de ICC-profielen zelf zijn "
"ingebed om compatibiliteit over verschillende computers te verzekeren."
