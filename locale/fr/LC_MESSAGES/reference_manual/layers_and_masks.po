msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-02-27 02:49+0100\n"
"Last-Translator: KDE Francophone <kde-francophone@kde.org>\n"
"Language-Team: KDE Francophone <kde-francophone@kde.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"
"X-Environment: kde\n"
"X-Language: fr_FR\n"
"X-Qt-Contexts: true\n"
"Generated-By: Babel 0.9.6\n"
"X-Source-Language: C\n"

#: ../../reference_manual/layers_and_masks.rst:5
msgid "Layers and Masks"
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:7
msgid "Layers are a central concept in digital painting."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:9
msgid ""
"With layers you can get better control over your artwork, for example you "
"can color an entire artwork just by working on the separate color layer and "
"thereby not destroying the line art which will reside above this color layer."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:11
msgid ""
"Furthermore, layers allow you to change the composition easier, and mass "
"transform certain elements at once."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:13
msgid ""
"Masks on the other hand allow you to selectively apply certain effects on a "
"layer, like transparency, transformation and filters."
msgstr ""

#: ../../reference_manual/layers_and_masks.rst:15
msgid "Check the :ref:`layers_and_masks` for more information."
msgstr ""
